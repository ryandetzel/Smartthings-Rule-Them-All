/**
 *  Rule Them All
 */
 
import groovy.transform.Field

@Field debugMissingMethods = true

/* Constants, because, constants are not avaiable */
def getTheaterAccentLight(){ return "Theater Accent Light" }
def getSonos(){ return "Sonos" }
def getChristmasLights(){ return "Christmas Lights" }
def getOn(){ return "on" }
def getOff(){ return "off" }
def getOpen(){ return "open" }
def getClosed(){ return "closed" }

/** 
* Personal script stuff remove if sharing
*/

@Field smsNumber = "" //TODO move this to a prefence page in app?

/**
* Helper Methods
*/

/* Turn lights on/off depending on what's going on
*  Called from various mode/presence changes
*/
private adjustLights(evt) {
    def currMode = location.mode
    if (someoneIsHome()){
        if (currMode != "Night"){
            ex(christmasLights, on)
        } else {
            ex(christmasLights, off)
        }
        ex(theaterAccentLight, on)
    } else {
        ex(christmasLights, off)
        ex(theaterAccentLight, off)
    }
}

private mainLightsAreOn() {
    // TODO clean this up, feels messy and error prone
    // Maybe make this a prefence pane in app?
    if (value("Hallway Light", "switch") == on ||
        value("Bathroom Light", "switch") == on ||
        value("Bedroom Light", "switch") == on ||
        value("Mudroom Light", "switch") == on ||
        value("Dining Light", "switch") == on){
        return true
    }
    return false
}

/**
* Switches
*/

private mudroomLight(evt) {
    // Turn sonos on/off with light
    ex(sonos, evt.value)
}

/** 
* Presence actions
*/
private everyoneIsAway(evt) {    
    adjustLights(evt)
    
    // Check in 15 minutes to see if we left a light on
    if (mainLightsAreOn())    runIn(60 * 15, followupEveryoneIsAway)
}

/**
* Follow ups
* These are called by other methods usually after a delay.
* They should not be called directly
* MUST BE PUBLIC
*/
def followupEveryoneIsAway(){
    log.debug "Followup everyoneIsAway"
    if (everyoneIsAway() && mainLightsAreOn()){
        sendSms(smsNumber, "You left a light on")
       }
}

/**
* Presence
*/
private everyoneIsHome(evt) {
    log.debug "everyone is home"
}

private someoneLeft(evt) {
    log.debug "someone has left ${evt.device.displayName}"
}

private someoneArrived(evt) {
    log.debug "someone has arrived ${evt.device.displayName}"
    adjustLights(evt)
}

/**
* Contact Sensors
*/

private pantryDoor(evt) {
    ex("Pantry Light", (evt.value == open) ? on:off)
}

/**
* Motion
*/

private motionOneActive(evt) {
    /* Motion detected in mudroom, turn on lights and music */
    ex("Sonos", "play")
    ex("Mudroom Light", "on")
    
    // if we're not home, send me a text
    if (everyoneIsAway()) {
        sendSms(smsNumber, "Motion detected in the mudroom")
    }
}

private motionOneInactive(evt) {
    /* Motion stopped in mudroom, shut things off */
    ex("Sonos", "pause")
    ex("Mudroom Light", "off")
}

/**
* Routines/Modes
*/

def morningModeExecuted(evt) {
    log.debug "Good morning mode executed"
    adjustLights(evt)
}

def nightModeExecuted(evt) {
    log.debug "Good night mode executed"
    adjustLights(evt)
}

def bedtimeRoutineExecuted(evt) {
    // It's bedtime
    
    //ex("Bedroom Light", "on")
    ex("Bathroom Light", "on")
    
    ex("Mudroom Light", "off")
    ex("Dining Light", "off")
    ex("TV", "off")
    ex(christmasLights, "off")
}

/**
* Handlers, should need to change any of this
*/

def _buttonHandler(evt){
    log.debug "${evt.value}  ${evt.data}  ${evt.description}"
}

def _contactHandler(evt){
    def actionHandler = action(evt.displayName)
    def value = evt.value.capitalize()
    
    try {
        "$actionHandler$value"(evt)
    } catch (MissingMethodException e){
        if (debugMissingMethods) log.debug "method ${actionHandler} not found - contact - ${e}"
    }
    
    try {
        "$actionHandler"(evt)
    } catch (MissingMethodException e){
        if (debugMissingMethods) log.debug "method ${actionHandler} not found - contact - ${e}"
    }
}

def _motionHandler(evt){
    def actionHandler = action(evt.displayName) + evt.value.capitalize()
    
    try {
        "$actionHandler"(evt)
    } catch (MissingMethodException e){
        if (debugMissingMethods) log.debug "method ${actionHandler} not found - motion"
    }
}

def _switchHandler(evt){
    def actionHandler = action(evt.displayName)
    def value = evt.value.capitalize()
    
    try {
        "$actionHandler$value"(evt)
    } catch (MissingMethodException e){
        if (debugMissingMethods) log.debug "method ${actionHandler}${value} not found - switch 1"
    }
    
    try {
        "$actionHandler"(evt)
    } catch (MissingMethodException ee){
        if (debugMissingMethods) log.debug "method ${actionHandler} not found - switch 2"
    }
}

def _presenceHandler(evt) {
    if (evt.value == "not present") {
        someoneLeft(evt)
        if (everyoneIsAway()) {
            everyoneIsAway(evt)
        }
    }
    else {
        someoneArrived(evt)
        if (everyoneIsHome()) {
            everyoneIsHome(evt)
        }
    }
}

def _routineChangedHandler(evt) {
    log.debug "Routined ${evt.displayName} executed"
    def actionHandler = action(evt.displayName) + "RoutineExecuted"
    try {
        "$actionHandler"(evt)
    } catch (MissingMethodException e){
        if (debugMissingMethods) log.debug "method ${actionHandler} not found - routine"
    }
}

def _modeChangeHandler(evt) {
    log.debug "Mode changed ${location.currentMode.name}"
    def actionHandler = action(location.currentMode.name) + "ModeExecuted"
    try {
        "$actionHandler"(evt)
    } catch (MissingMethodException e){
        if (debugMissingMethods) log.debug "method ${actionHandler} not found - mood"
    }
}

/**
* Helper methods
*/

private action(name) {
    return "${cleanName(name)[0].toLowerCase()}${cleanName(name).substring(1)}"
}

private cleanName(name) {
    return name.replaceAll('[^a-zA-Z0-9]+','')
}

private someoneIsHome() {
    for (person in people) {
        if (person.currentPresence == "present") {
            return true
        }
    }
    return false
}

private everyoneIsHome() {
    for (person in people) {
        if (person.currentPresence == "not present") {
            return false
        }
    }
    return true
}

private everyoneIsAway() {
    for (person in people) {
        if (person.currentPresence == "present") {
            return false
        }
    }
    return true
}

def installed() {
    log.debug "Installed with settings: ${settings}"
    initialize()
}

def updated() {
    log.debug "Updated with settings: ${settings}"
    unsubscribe()
    initialize()
}

/**
* These two methods help you call methods on devices.
* It's safer to use the first one than the second because if you use deviceWithName
* and the device does not exist the app will crash unless you check for null
    ex("Fireplace Left", "setLevel", [100])
    deviceWithName("Bathroom Light").on()
    deviceWithName("Bathroom lligh")?.on()
*/

private ex(name, command, args = []) {
    log.debug "sent '${name}' command '${command}' with arguments ${args}"
    device = isValidCommand(name, command)
    device?."$command"(*args)
}

private deviceWithName(name){
    for (sw in switches + music + motion + people + levels + buttons + colors) {
        if (sw?.displayName == name){
            return sw
        }
    }
    log.error "Device with name ${name} not found"
    return null
}

private isValidCommand(name, command) {
    def device = deviceWithName(name)
    def valid = false
    if (device != null){
        device.capabilities.commands.each {comm ->
            if (command in comm.name){
                valid = true
            }
        }
    }
    if (valid) {
        return device
    } else {
        log.error "Command ${name} ${command} is not valid"
        return null
    }
}

private value(name, attribute){
    device = deviceWithName(name)
    if (device != null){
        //TODO check if it's a valid attr
        //def theAtts = device.supportedAttributes
           return device.currentValue(attribute.toLowerCase())
    }
    return null
}

/**
* Normal ST stuff
*/
definition(
    name: "Rule Them All",
    namespace: "ryanrdetzel",
    author: "Ryan Detzel",
    description: "One Smart App To Rule Them All",
    category: "",
    singleInstance: true,
    iconUrl: "https://s3.amazonaws.com/smartapp-icons/Convenience/Cat-Convenience.png",
    iconX2Url: "https://s3.amazonaws.com/smartapp-icons/Convenience/Cat-Convenience@2x.png",
    iconX3Url: "https://s3.amazonaws.com/smartapp-icons/Convenience/Cat-Convenience@2x.png")


preferences {
    section("Switches") {
        input "switches", "capability.switch", multiple: true, required: false
    }
    section ("Switch Level") {
        input "levels", "capability.switchLevel", multiple: true, required: false
    }
    section("Color Control") {
        input "colors", "capability.colorControl", multiple: true, required: false
    }
    section("Switches Buttons") {
        input "buttons", "capability.button", multiple: true, required: false
    }
    section("Presense") {
        input "people", "capability.presenceSensor", multiple: true, required: false
    }
    section("Motion") {
        input "motion", "capability.motionSensor", multiple: true, required: false
    }
    section("Music") {
        input "music", "capability.musicPlayer", multiple: true, required: false
    }
    section("Contact") {
        input "contact", "capability.contactSensor", multiple: true, required: false
    }
}

def initialize() {
    subscribe(switches, "switch", _switchHandler)
    subscribe(people, "presence", _presenceHandler)
    subscribe(motion, "motion", _motionHandler)
    subscribe(buttons, "button", _buttonHandler)
    subscribe(contact, "contact", _contactHandler)
    
    subscribe(location, "routineExecuted", _routineChangedHandler)
    subscribe(location, "mode", _modeChangeHandler)
}
